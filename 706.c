#include <stdio.h>

int main(){
    
    int s;
    int l;
    char c;

    int dig[10];

    while(scanf("%d",&s)!=EOF){
        l = 0;
        do{
            c = getchar();
            if(c>='0'&&c<='9'){
                dig[l++] = c-'0';
            }
        }while(c!='\n');

        if(s==0&&l==1&&dig[0]==0){
            break;
        }else{
            //Output
            for(int i=0;i<2*s+3;i++){
                for(int j=0;j<l;j++){
                    if(i==0){
                        switch(dig[j]){
                            case 1:
                            case 4:
                                for(int k=0;k<2+s;k++){
                                    printf(" ");
                                }
                                break;
                            default:
                                printf(" ");
                                for(int k=0;k<s;k++){
                                    printf("-");
                                }
                                printf(" ");
                                break;
                        } 
                    }else if(i<s+1){
                        switch(dig[j]){
                            case 1:
                            case 2:
                            case 3:
                            case 7:
                                for(int k=0;k<1+s;k++){
                                    printf(" ");
                                }
                                printf("|");
                                break;
                            case 4:
                            case 8:
                            case 9:
                            case 0:
                                printf("|");
                                for(int k=0;k<s;k++){
                                    printf(" ");
                                }
                                printf("|");
                                break;
                            default:
                                printf("|");
                                for(int k=0;k<1+s;k++){
                                    printf(" ");
                                }
                                break;
                        } 
                    }else if(i==s+1){
                        switch(dig[j]){
                            case 1:
                            case 7:
                            case 0:
                                for(int k=0;k<2+s;k++){
                                    printf(" ");
                                }
                                break;
                            default:
                                printf(" ");
                                for(int k=0;k<s;k++){
                                    printf("-");
                                }
                                printf(" ");
                                break;
                        } 
                    }else if(i<2*s+2){
                        switch(dig[j]){
                            case 1:
                            case 3:
                            case 4:
                            case 5:
                            case 7:
                            case 9:
                                for(int k=0;k<1+s;k++){
                                    printf(" ");
                                }
                                printf("|");
                                break;
                            case 2:
                                printf("|");
                                for(int k=0;k<1+s;k++){
                                    printf(" ");
                                }
                                break;
                            default:
                                printf("|");
                                for(int k=0;k<s;k++){
                                    printf(" ");
                                }
                                printf("|");
                                break;
                        } 
                    }else{
                        switch(dig[j]){
                            case 1:
                            case 4:
                            case 7:
                                for(int k=0;k<2+s;k++){
                                    printf(" ");
                                }
                                break;
                            default:
                                printf(" ");
                                for(int k=0;k<s;k++){
                                    printf("-");
                                }
                                printf(" ");
                                break;
                        } 
                    }
                    
                    if(j!=l-1){
                        printf(" ");
                    }
                }
                printf("\n");
            }
            printf("\n");
        }

    }

    return 0;
}
