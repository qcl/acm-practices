#include <stdio.h>
#include <math.h>

int main(){
    unsigned long int n;
    double k;

    while(scanf("%lu",&n)!=EOF){
        if(n==0){
            break;
        }else{
            k = sqrt(n);
            if(floor(k)==ceil(k)){
                printf("yes\n",k);
            }else{
                printf("no\n");
            }
        }
    }
    return 0;
}
