#include <stdio.h>

int main(){

    int i;
    int j;
    int k;

    int n;
    int s;
    int p;
    int q;

    int map[100][100];
    int dist[100];

    int hasChange;
    int path = 0;
    int end  = 0;
    int caseN = 0;

    while(scanf("%d",&n)!=EOF){
        if(n==0){
            break;
        }

        //init
        for(i=0;i<n;i++){
            dist[i] = 0;
            for(j=0;j<n;j++){
                map[i][j] = 0;
            }
        }
        path = 0;

        //read data
        scanf("%d",&s); //start point
        
        do{
            scanf("%d %d",&p,&q);
            map[p-1][q-1] = 1;

            if(p==s){
                dist[q-1] = 1;
            }
            
        }while(p!=0&&q!=0);

        for(k=0;k<n;k++){   //at most n steps.
            hasChange = 0;
            for(i=0;i<n;i++){
                if(dist[i]>0){
                    for(j=0;j<n;j++){
                        if(map[i][j]==1&&dist[j]<dist[i]+1){
                            hasChange = 1;
                            dist[j] = dist[i] + 1;
                        }
                    }
                } 
            }
            if(hasChange==0){
                break;
            }
        }

        //find the longest path.
        for(i=n-1;i>=0;i--){
            if(dist[i]>=path){
                path = dist[i];
                end = i+1;    
            }
            //printf("%d %d\n",i,dist[i]);
        }

        printf("Case %d: The longest path from %d has length %d, finishing at %d.\n\n",++caseN,s,path,end);
    }

    return 0;
}
